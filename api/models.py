from django.db import models

# Create your models here.

class Snippet(models.Model):
	entry_id = models.CharField(max_length=50)
	content = models.CharField(max_length=10000)
	#version_id = models.IntegerField()

	def __unicode__(self):
		return self.entry_id