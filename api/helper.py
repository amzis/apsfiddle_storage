from api.models import Snippet

import string
import random

def generate_id():
        length = 8
        attempts = 5
        chars = string.ascii_lowercase + string.digits

        for i in range(attempts):
                proposed_id = ''.join(random.choice(chars) for i in range(length))
		if not Snippet.objects.filter(entry_id=proposed_id):
			return proposed_id
