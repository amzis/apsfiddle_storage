from django.conf.urls import patterns, url
from api import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^(?P<entry_id>[^/]+)/$', views.entry_crud, name='entry_crud'),
)