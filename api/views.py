from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from api.models import Snippet

import json

import helper

def index(request):

    if request.method == 'OPTIONS':
        return HttpResponse(status=200)

    elif request.method == 'POST':
        try:
            data = json.loads(request.body)
        except:
            return JsonResponse({'error': 'content should be sent in json'}, status=400)
        try:
            snippet = Snippet(entry_id = helper.generate_id(), content = data['content'])
        except KeyError:
            return JsonResponse({'error': 'request should contain \'content\' property'}, status=400)
        snippet.save()
        return JsonResponse({'entry_id': str(snippet.entry_id)})

    else:
        return JsonResponse({'error': 'method not implemented'}, status=400)

def entry_crud(request, entry_id):

    snippet = get_object_or_404(Snippet, entry_id=entry_id)

    if request.method == 'OPTIONS':
       return HttpResponse(status=200)

    elif request.method == 'GET':
        return JsonResponse({ 'content': json.dumps(snippet.content)})

    # TODO: decided it will not be used via API, need to move to server-side crean-up cron
    elif request.method == 'DELETE':
        snippet.delete()
        return HttpResponse(status=200)

    else:
        return JsonResponse({'error': 'method not implemented'}, status=400)

